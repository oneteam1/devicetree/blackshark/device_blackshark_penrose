#
# Copyright (C) 2018-2021 ArrowOS
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit common products
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit device configurations
$(call inherit-product, device/blackshark/penrose/device.mk)

# Inherit common PE configurations
$(call inherit-product, vendor/aosp/config/common_full_phone.mk)

PRODUCT_CHARACTERISTICS := nosdcard

PRODUCT_BRAND := blackshark
PRODUCT_DEVICE := penrose
PRODUCT_MANUFACTURER := blackshark
PRODUCT_MODEL := SHARK PRS-H0
PRODUCT_NAME := aosp_penrose

TARGET_FACE_UNLOCK_SUPPORTED := true
TARGET_GAPPS_ARCH := arm64
PRODUCT_GMS_CLIENTID_BASE := android-blackshark
TARGET_SUPPORTS_QUICK_TAP := true

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="penrose-user 11 PROS2110220OS00MP3 V12.0.1.0.RBICNBS release-keys"

BUILD_FINGERPRINT := blackshark/PRS-H0/penrose:11/PROS2110220OS00MP3/V12.0.1.0.RBICNBS:user/release-keys

ONEPIXEL_BUILD_TYPE := OFFICIAL
#PRODUCT_BUILD_PROP_OVERRIDES += \
#    PRIVATE_BUILD_DESC="penrose-user 11 PROS2201111CN00MR1 V12.0.1.0.RBICNBS release-keys" \
#    PRODUCT_NAME=vayu_global \
#    PRODUCT_MODEL="SHARK PRS-A0"

#BUILD_FINGERPRINT := blackshark/PRS-A0/penrose:11/PROS2201111CN00MR1/V12.0.1.0.RBICNBS:user/release-keys
